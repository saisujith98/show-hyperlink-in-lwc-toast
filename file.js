var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    mode: 'sticky',
                    message: 'This is a required message',
                    messageTemplate: 'Record {0} created! See it {1}!',
                    messageTemplateData: ['Salesforce', {
                    **url: 'https://xx-dev-ed.lightning.force.com/one/one.app?source=alohaHeader#/sObject/'**+recId,
                    label: 'here',
                }]

            });
            resultsToast.fire();